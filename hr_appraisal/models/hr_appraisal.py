# -*- coding:utf-8 -*-

import time
from datetime import datetime, timedelta
from dateutil import relativedelta

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError, ValidationError


class HrEmployee(models.Model):
    """ Inherit Employee module to add new fields that will be used in appraisal """
    _inherit = 'hr.employee'

    # Field Declaration
    emp_no = fields.Char(_('Employee No.'))
    first_working_date = fields.Date(_('First Working Date'))
    is_probation = fields.Boolean(_('At Probation?'))


class ProbationAppraisalDetails(models.Model):
    """ This module for probation appraisal detail (this will used for criteria) """
    _name = 'probation.appraisal.details'

    name = fields.Char(_('Criteria'))
    score = fields.Selection([(1, '1'),
                              (2, '2'),
                              (3, '3'),
                              (4, '4'),
                              (5, '5')], _('Score'))
    appraisal_id = fields.Many2one('probation.appraisal')


class AnnualAppraisalDetails(models.Model):
    """ This module for annual appraisal detail (this will used for criteria) """
    _name = 'annual.appraisal.details'

    name = fields.Char(_('Criteria'))
    score = fields.Selection([('below_satisfactory', _('Below Satisfactory')),
                              ('satisfactory', _('Satisfactory')),
                              ('excellent', _('Excellent'))], _('Score'))
    appraisal_id = fields.Many2one('annual.appraisal')


class ProbationAppraisal(models.Model):
    _name = 'probation.appraisal'
    _description = 'Probation Appraisal'
    _inherit = ['mail.thread']

    @api.model
    def _get_default_template(self):
        _, annual_template = self.env['ir.model.data'].get_object_reference('hr_appraisal',
                                                                            'default_probation_template')
        return annual_template

    # Field Deceleration
    employee_id = fields.Many2one('hr.employee', _('Employee'))
    project_id = fields.Many2one('project.project', _('Project'))
    date = fields.Date(_('Date'))
    appraisal_template_id = fields.Many2one('appraisal.template', _('Appraisal Template'),
                                            default=lambda self: self._get_default_template())
    contract_start_date = fields.Date(_('Contract Start Date'), compute='_compute_employee_data')
    contract_end_date = fields.Date(_('Contract End Date'), compute='_compute_employee_data')
    line_manager_id = fields.Many2one('hr.employee', _('Direct Manager'), compute='_compute_employee_data')
    job_id = fields.Many2one('hr.job', _('Position'), compute='_compute_employee_data')
    department_id = fields.Many2one('hr.department', _('Department'), compute='_compute_employee_data')
    probation_appraisal_details_ids = fields.One2many('probation.appraisal.details', 'appraisal_id',
                                                      _('Probation Appraisal Details'))
    probation_type = fields.Selection([('30', '30'),
                                       ('60', '60'),
                                       ('80', '80')], _('Probation Type'))
    overall_assessment = fields.Selection([('excellent', _('Excellent')),
                                           ('good', _('Good')),
                                           ('satisfactory', _('Satisfactory')),
                                           ('below_expectation', _('Below Expectation')),
                                           ('unsatisfactory', _('Unsatisfactory'))],
                                          _('Overall Assessment'), compute='_compute_assessment')
    total_score = fields.Float(_('Total Score'), compute='_compute_assessment')
    assessment_of_employee_performance = fields.Text(_('Assessment of Employee Performance'))
    line_manager_recommendation = fields.Selection([('recommend_continued', _(
        'I recommend that this probationary employee can be continued employed by Norconsult Telematics')),
                                                    ('recommend_extend',
                                                     _('I recommend that the probationary period be extended')),
                                                    ('do_not_recommend', _(
                                                        'I do not recommend that this probationary employee be continued employed by Norconsult Telematics and that termination of contract procedures be instituted'))],
                                                   _('Overall Assessment'))
    line_manager_name = fields.Char(_('Line Manager Name'))
    line_manager_date = fields.Datetime(_('Line Manager Sign Date'))
    program_manager_name = fields.Char(_('Program Manager Name'))
    program_manager_date = fields.Datetime(_('Program Manager Sign Date'))
    general_manager_name = fields.Char(_('General Manager Name'))
    general_manager_date = fields.Datetime(_('General Manager Sign Date'))
    flag = fields.Boolean()
    refuse_notes = fields.Text(_('Refusal Notes'))
    state = fields.Selection([('draft', _('Draft')),
                              ('line_mgr', _('Line Manager')),
                              ('program_mgr', _('Program Manager')),
                              ('general_mgr', _('General Manager')),
                              ('done', _('Done')),
                              ('refuse', _('Refused'))], _('State'), default='draft')

    @api.depends('employee_id')
    def _compute_employee_data(self):
        for record in self:
            record.contract_start_date = record.employee_id.sudo().contract_id.date_start
            record.contract_end_date = record.employee_id.sudo().contract_id.date_end
            record.line_manager_id = record.employee_id.sudo().coach_id.id
            record.job_id = record.employee_id.sudo().job_id.id
            record.department_id = record.employee_id.sudo().department_id.id

    @api.depends('probation_appraisal_details_ids')
    def _compute_assessment(self):
        """ Function to compute assessment from criteria """
        for record in self:
            total_score = 0
            for criteria in record.probation_appraisal_details_ids:
                total_score += int(criteria.score)
            record.total_score = total_score
            if 60 >= total_score >= 48:
                record.overall_assessment = 'excellent'
            elif 48 > total_score >= 36:
                record.overall_assessment = 'good'
            elif 36 > total_score >= 24:
                record.overall_assessment = 'satisfactory'
            elif 24 > total_score >= 12:
                record.overall_assessment = 'below_expectation'
            elif 12 > total_score >= 0:
                record.overall_assessment = 'unsatisfactory'

    @api.constrains('employee_id')
    def _check_coach_id(self):
        """ To make sure that there is direct manager assigned to this employee """
        if not self.employee_id.coach_id:
            raise ValidationError(_('You have to set direct manager to employee !'))

    @api.constrains('employee_id', 'probation_type')
    def _check_duplication(self):
        for appraisal in self:
            domain = [
                ('employee_id', '=', appraisal.employee_id.id),
                ('id', '!=', appraisal.id),
                ('state', '!=', 'refuse'),
                ('probation_type', '=', appraisal.probation_type),
            ]
            nappraisal = self.search_count(domain)
            if nappraisal:
                raise ValidationError(_('You can not have 2 appraisals to same employee with same probation type!'))

    @api.constrains('employee_id')
    def _check_direct_manager(self):
        """ Check the use who create the record is the DM or Admin (because of creation from scheduler """
        if not self.env.user.has_group('base.group_system'):
            if self.employee_id.coach_id.user_id:
                if self.env.user.id != self.employee_id.coach_id.user_id.id:
                    raise ValidationError(_('Sorry, you have to be direct manager to employee'))

    @api.constrains('employee_id', 'probation_type')
    def check_probation_type(self):
        """ Check the probation type fit the actual probation period """
        if self.employee_id and self.probation_type:
            if not self.employee_id.contract_id:
                raise ValidationError(_('Kindly, You have to create contract to this employee'))
            if not self.employee_id.sudo().contract_id.trial_date_start:
                raise ValidationError(_('Kindly, You have to set trail period duration in employee contract'))
            trial_date_start = datetime.strptime(self.employee_id.sudo().contract_id.trial_date_start,
                                                 '%Y-%m-%d').date()
            date = datetime.strptime(self.date, '%Y-%m-%d').date()
            diff = (date - trial_date_start).days
            if self.probation_type == '30' and diff < 30:
                raise ValidationError(_('Probation of 30 should be after 30 days of start trial period'))
            if self.probation_type == '60' and diff < 60:
                raise ValidationError(_('Probation of 60 should be after 60 days of start trial period'))
            if self.probation_type == '80' and diff < 80:
                raise ValidationError(_('Probation of 80 should be after 80 days of start trial period'))

    @api.one
    def fill_criteria(self):
        for criteria in self.appraisal_template_id.template_criteria_ids:
            self.env['probation.appraisal.details'].create({'name': criteria.name,
                                                            'appraisal_id': self.id})
        self.flag = True

    @api.multi
    @api.depends('employee_id')
    def name_get(self):
        return [(r.id, ("Appraisal for %s" % r.employee_id.name)) for r in self]

    @api.one
    def action_line_mgr(self):
        if not self.flag:
            raise UserError(_('You have to fill criteria template'))
        if not self.assessment_of_employee_performance:
            raise UserError(_('You have to fill Assessment of Employee Performance'))
        if not self.line_manager_recommendation:
            raise UserError(_('You have to chose Overall assessment'))
        for criteria in self.probation_appraisal_details_ids:
            if not criteria.score:
                raise UserError(_('You can not submit this record while there is a line without score'))
        users = []
        for user in self.env['res.users'].search([]):
            if user.has_group('hr_appraisal.group_program_manager'):
                users.append(user.partner_id.id)
        if len(users) > 0:
            msg = _('Kindly, Put your approval probation appraisal')
            self.message_post(body=msg, partner_ids=users)
        self.state = 'line_mgr'
        self.line_manager_date = datetime.now()
        self.line_manager_name = self.env.user.name

    @api.one
    def action_program_mgr(self):
        users = []
        for user in self.env['res.users'].search([]):
            if user.has_group('hr_appraisal.group_general_manager'):
                users.append(user.partner_id.id)
        if len(users) > 0:
            msg = _('Kindly, Put your approval probation appraisal')
            self.message_post(body=msg, partner_ids=users)
        self.state = 'program_mgr'
        self.program_manager_date = datetime.now()
        self.program_manager_name = self.env.user.name

    @api.one
    def action_general_mgr(self):
        self.state = 'done'
        self.general_manager_date = datetime.now()
        self.general_manager_name = self.env.user.name
        users = [self.create_uid.partner_id.id]
        msg = _('Kindly, Appraisal had been Approved')
        self.message_post(body=msg, partner_ids=users)

    @api.one
    def action_refuse(self):
        if not self.refuse_notes:
            raise ValidationError(_('You have to write refuse notes'))
        users = []
        for user in self.env['res.users'].search([]):
            if user.has_group('hr_appraisal.group_program_manager'):
                users.append(user.partner_id.id)
        users.append(self.create_uid.partner_id.id)
        msg = _('Kindly, Appraisal had been refused')
        self.message_post(body=msg, partner_ids=users)
        self.state = 'refuse'

    @api.multi
    def unlink(self):
        if self.filtered(lambda x: x.state != 'draft'):
            raise UserError(
                _('You can not remove a record not in draft state'))
        return super(ProbationAppraisal, self).unlink()

    @api.model
    def create(self, vals):
        obj = super(ProbationAppraisal, self).create(vals)
        obj.message_post(body=_('%s had created appraisal') % self.env.user.name)
        return obj

    @api.model
    def _probation_notification(self):
        """ Scheduler Method """
        users = []
        # Get users for GM and HR to add them in notification
        for user in self.env['res.users'].search([]):
            if user.has_group('hr_appraisal.group_general_manager'):
                users.append(user.partner_id.id)
            if user.has_group('hr.group_hr_manager'):
                users.append(user.partner_id.id)
        # Search for employee contract that in probation period
        for contract in self.env['hr.contract'].search([('trial_date_start', '!=', False)]):
            if contract.trial_date_start:
                trail_start = datetime.strptime(contract.trial_date_start, '%Y-%m-%d').date()
                today_date = datetime.now().date()
                diff_days = (today_date - trail_start).days
                if 30 <= diff_days < 60:
                    nappraisal = self.search_count(
                        [('employee_id', '=', contract.employee_id.id), ('probation_type', '=', '30')])
                    if nappraisal < 1:
                        vals = {
                            'employee_id': contract.employee_id.id,
                            'date': today_date,
                            'probation_type': '30',
                        }
                        obj = self.create(vals)
                        body = _('You have to start probation appraisal for %s') % contract.employee_id.name
                        if contract.employee_id.coach_id.user_id:
                            partner_ids = [contract.employee_id.coach_id.user_id.partner_id.id]
                            obj.message_post(body=body, partner_ids=partner_ids)
                elif 60 <= diff_days < 80:
                    nappraisal = self.search_count(
                        [('employee_id', '=', contract.employee_id.id), ('probation_type', '=', '60')])
                    if nappraisal < 1:
                        vals = {
                            'employee_id': contract.employee_id.id,
                            'date': today_date,
                            'probation_type': '60',
                        }
                        obj = self.create(vals)
                        body = _('You have to start probation appraisal for %s') % contract.employee_id.name
                        if contract.employee_id.coach_id.user_id:
                            partner_ids = [contract.employee_id.coach_id.user_id.partner_id.id]
                            obj.message_post(body=body, partner_ids=partner_ids)
                elif diff_days >= 80:
                    nappraisal = self.search_count(
                        [('employee_id', '=', contract.employee_id.id), ('probation_type', '=', '80')])
                    if nappraisal < 1:
                        vals = {
                            'employee_id': contract.employee_id.id,
                            'date': today_date,
                            'probation_type': '80',
                        }
                        obj = self.create(vals)
                        body = _('You have to start probation appraisal for %s') % contract.employee_id.name
                        if contract.employee_id.coach_id.user_id:
                            partner_ids = [contract.employee_id.coach_id.user_id.partner_id.id]
                            obj.message_post(body=body, partner_ids=partner_ids)
        # Search for draft probation appraisal and if it date more than 5 days it send notification
        for appraisal in self.search([('state', '=', 'draft')]):
            appraisal_date = datetime.strptime(appraisal.date, '%Y-%m-%d').date()
            today_date = datetime.now().date()
            diff_days = (today_date - appraisal_date).days
            if diff_days >= 5:
                partner_ids = users
                body = _('You have to finish probation appraisal for %s') % appraisal.employee_id.name
                if appraisal.employee_id.coach_id.user_id:
                    partner_ids.append(appraisal.employee_id.coach_id.user_id.partner_id.id)
                appraisal.message_post(body=body, partner_ids=partner_ids)


class AnnualAppraisal(models.Model):
    _name = 'annual.appraisal'
    _description = 'Annual Appraisal'
    _inherit = ['mail.thread']

    @api.model
    def _get_default_template(self):
        _, annual_template = self.env['ir.model.data'].get_object_reference('hr_appraisal', 'default_annual_template')
        return annual_template

    employee_id = fields.Many2one('hr.employee', _('Employee'))
    project_id = fields.Many2one('project.project', _('Project'))
    date = fields.Date(_('Date'))
    year = fields.Integer(_('Year'), compute='_compute_date_year')
    appraisal_template_id = fields.Many2one('appraisal.template', _('Appraisal Template'),
                                            default=lambda self: self._get_default_template())
    contract_start_date = fields.Date(_('Contract Start Date'), compute='_compute_employee_data')
    contract_end_date = fields.Date(_('Contract End Date'), compute='_compute_employee_data')
    line_manager_id = fields.Many2one('hr.employee', _('Direct Manager'), compute='_compute_employee_data')
    job_id = fields.Many2one('hr.job', _('Position'), compute='_compute_employee_data')
    department_id = fields.Many2one('hr.department', _('Department'), compute='_compute_employee_data')
    employed_since = fields.Date(_('Employed Since'), compute='_compute_employee_data')
    remark = fields.Char(_('Remark'))
    annual_appraisal_details_ids = fields.One2many('annual.appraisal.details', 'appraisal_id',
                                                   _('Annual Appraisal Details'))
    overall_assessment = fields.Selection([('excellent', _('Excellent')),
                                           ('satisfactory', _('Satisfactory')),
                                           ('below_expectation', _('Below Expectation'))],
                                          _('Overall Assessment'), compute='_compute_assessment')
    assessment_of_employee_performance = fields.Text(_('Assessment of Employee Performance'))
    other_useful_skills = fields.Text(_('Other Skills Useful in Other Positions'))
    line_manager_name = fields.Char(_('Line Manager Name'))
    line_manager_date = fields.Datetime(_('Line Manager Sign Date'))
    project_manager_name = fields.Char(_('Project Manager Name'))
    project_manager_date = fields.Datetime(_('Project Manager Sign Date'))
    flag = fields.Boolean()
    refuse_notes = fields.Text(_('Refuse Notes'))
    state = fields.Selection([('draft', _('Draft')),
                              ('line_mgr', _('Line Manager')),
                              ('project_mgr', _('Project Manager')),
                              ('done', _('Done')),
                              ('refuse', _('Refused'))], _('State'), default='draft')

    @api.depends('employee_id')
    def _compute_employee_data(self):
        for record in self:
            record.contract_start_date = record.employee_id.sudo().contract_id.date_start
            record.contract_end_date = record.employee_id.sudo().contract_id.date_end
            record.line_manager_id = record.employee_id.sudo().coach_id.id
            record.job_id = record.employee_id.sudo().job_id.id
            record.department_id = record.employee_id.sudo().department_id.id

    @api.depends('annual_appraisal_details_ids')
    def _compute_assessment(self):
        for record in self:
            total_excellent = 0
            total_satisfactory = 0
            total_below_expectation = 0
            for criteria in record.annual_appraisal_details_ids:
                if criteria.score == 'excellent':
                    total_excellent += 1
                elif criteria.score == 'satisfactory':
                    total_satisfactory += 1
                else:
                    total_below_expectation += 1
            if total_excellent > total_satisfactory and total_excellent > total_below_expectation:
                record.overall_assessment = 'excellent'
            elif total_satisfactory > total_excellent and total_satisfactory > total_below_expectation:
                record.overall_assessment = 'satisfactory'
            elif total_below_expectation > total_excellent and total_below_expectation > total_satisfactory:
                record.overall_assessment = 'below_expectation'

    @api.one
    def fill_criteria(self):
        for criteria in self.appraisal_template_id.template_criteria_ids:
            self.env['annual.appraisal.details'].create({'name': criteria.name,
                                                         'appraisal_id': self.id})
        self.flag = True

    @api.depends('date')
    def _compute_date_year(self):
        for appraisal in self:
            if appraisal.date:
                date_obj = datetime.strptime(self.date, '%Y-%m-%d').date()
                year = date_obj.year
                appraisal.year = year

    @api.constrains('employee_id')
    def _check_coach_id(self):
        if not self.employee_id.coach_id:
            raise ValidationError(_('You have to sit direct manager to employee !'))

    @api.constrains('employee_id')
    def _check_direct_manager(self):
        if self.employee_id.coach_id.user_id:
            if self.env.user.id != self.employee_id.coach_id.user_id.id:
                raise ValidationError(_('Sorry, you have to be direct manager to employee'))

    @api.constrains('date')
    def _check_date(self):
        for appraisal in self:
            date_obj = datetime.strptime(self.date, '%Y-%m-%d').date()
            year = date_obj.year
            appraisal_start = datetime.strptime('%d-11-01' % year, '%Y-%m-%d').date()
            appraisal_end = datetime.strptime('%d-12-31' % year, '%Y-%m-%d').date()
            if not appraisal_start <= date_obj <= appraisal_end:
                raise ValidationError(_('Annual appraisal created from Nov. till December!'))
            domain = [
                ('date', '<=', appraisal_end),
                ('date', '>=', appraisal_start),
                ('employee_id', '=', appraisal.employee_id.id),
                ('id', '!=', appraisal.id),
                ('state', '!=', 'refuse'),
            ]
            nappraisal = self.search_count(domain)
            if nappraisal:
                raise ValidationError(_('You can not have 2 appraisals that overlaps on same year!'))

    @api.multi
    @api.depends('employee_id')
    def name_get(self):
        return [(r.id, ("Appraisal for %s" % r.employee_id.name)) for r in self]

    @api.one
    def action_line_mgr(self):
        if not self.flag:
            raise UserError(_('You have to fill criteria template'))
        for criteria in self.annual_appraisal_details_ids:
            if not criteria.score:
                raise UserError(_('You can not submit this record while there is a line without score'))
        users = []
        for user in self.env['res.users'].search([]):
            if user.has_group('hr_appraisal.group_project_manager'):
                users.append(user.partner_id.id)
        if len(users) > 0:
            msg = _('Kindly, Put your approval annual appraisal')
            self.message_post(body=msg)
        self.state = 'line_mgr'
        self.line_manager_date = datetime.now()
        self.line_manager_name = self.env.user.name

    @api.one
    def action_project_mgr(self):
        self.state = 'done'
        self.project_manager_date = datetime.now()
        self.project_manager_name = self.env.user.name
        #  To create employee appraisal details
        self.env['employee.appraisal.details'].create({'emp_no': self.employee_id.emp_no,
                                                       'employee_id': self.employee_id.id,
                                                       'date': self.date,
                                                       'grade': self.overall_assessment,
                                                       'appraisal_id': self.id})
        # To create manager appraisal details
        created_record = self.env['manager.appraisal.details'].search([('employee_id', '=', self.line_manager_id.id),
                                                                       ('year', '=', self.year)])
        no_of_employees = self.env['hr.employee'].search_count([('coach_id', '=', self.line_manager_id.id),
                                                                ('is_probation', '=', False)])
        if len(created_record) > 0:
            created_record[0].write({
                'assessed': created_record[0].assessed + 1,
                'no_of_employees': no_of_employees,
            })
        else:
            self.env['manager.appraisal.details'].create({'emp_no': self.line_manager_id.emp_no,
                                                          'employee_id': self.line_manager_id.id,
                                                          'job_id': self.line_manager_id.job_id.id,
                                                          'department_id': self.line_manager_id.department_id.id,
                                                          'year': self.year,
                                                          'no_of_employees': no_of_employees,
                                                          'assessed': 1})
        users = [self.create_uid.partner_id.id]
        msg = _('Kindly, Appraisal had been Approved')
        self.message_post(body=msg, partner_ids=users)

    @api.one
    def action_refuse(self):
        if not self.refuse_notes:
            raise ValidationError(_('You have to write refuse note'))
        users = []
        for user in self.env['res.users'].search([]):
            if user.has_group('hr_appraisal.group_project_manager'):
                users.append(user.partner_id.id)
        users.append(self.create_uid.partner_id.id)
        msg = _('Kindly, Appraisal had been refused')
        self.message_post(body=msg, partner_ids=users)
        self.state = 'refuse'

    @api.multi
    def unlink(self):
        if self.filtered(lambda x: x.state != 'draft'):
            raise UserError(_('You can not remove a record not in draft state'))
        return super(AnnualAppraisal, self).unlink()

    @api.model
    def create(self, vals):
        obj = super(AnnualAppraisal, self).create(vals)
        obj.message_post(body=_('%s had created appraisal') % self.env.user.name)
        return obj

    @api.model
    def _annual_notification(self):
        today_date = datetime.now().date()
        current_year = today_date.year
        first_of_nov = datetime.strptime('%d-11-01' % current_year, '%Y-%m-%d').date()
        if first_of_nov == today_date:
            for contract in self.env['hr.contract'].search([('active', '=', True),
                                                            ('date_start', '<=', first_of_nov),
                                                            ('date_end', '>=', first_of_nov)]):
                body = _('You have to start annual appraisal for %s') % contract.employee_id.name
                if contract.employee_id.coach_id.user_id:
                    partner_ids = [contract.employee_id.coach_id.user_id.partner_id.id]
                    self.env['mail.thread'].message_post(body=body,
                                                         partner_ids=partner_ids)
