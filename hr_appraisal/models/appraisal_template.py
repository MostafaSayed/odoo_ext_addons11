# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _


class TemplateCriteria(models.Model):
    _name = 'template.criteria'

    name = fields.Char(_('name'))
    template_id = fields.Many2one('template_id', _('Template'))


class AppraisalTemplate(models.Model):
    _name = 'appraisal.template'

    name = fields.Char(_('Template Name'))
    type = fields.Selection([('probation', _('Probation')),
                             ('annual', _('Annual'))], _('Template Type'))
    template_criteria_ids = fields.One2many('template.criteria', 'template_id', _('Template Criteria'))

    _sql_constraints = [
        ('name_unique', 'UNIQUE(name, type)', _('The template name must be unique within a type'))
    ]