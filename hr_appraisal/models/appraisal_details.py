# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools, _


class EmployeeAppraisalDetails(models.Model):
    _name = 'employee.appraisal.details'

    emp_no = fields.Char(_('Employee No'))
    employee_id = fields.Many2one('hr.employee', _('Employee Name'))
    date = fields.Date(_('Appraisal Date'))
    grade = fields.Char(_('Grade'))
    appraisal_id = fields.Many2one('annual.appraisal', _('Appraisal'))

    @api.multi
    def open_employee_appraisal(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Employee Appraisal',
            'res_model': 'annual.appraisal',
            'view_type': 'form',
            'view_mode': 'form',
            'view_id': False,
            'res_id': self.appraisal_id.id
        }


class ManagerAppraisalDetails(models.Model):
    _name = 'manager.appraisal.details'

    emp_no = fields.Char(_('ID'))
    employee_id = fields.Many2one('hr.employee', _('Name'))
    year = fields.Char(_('Year'))
    job_id = fields.Many2one('hr.job', _('Position'))
    department_id = fields.Many2one('hr.department', _('Department'))
    no_of_employees = fields.Integer(_('No of Employees'))
    assessed = fields.Integer(_('Assessed'))
    remaining = fields.Integer(_('Remaining'), compute='_compute_remaining')

    @api.depends('no_of_employees', 'assessed')
    def _compute_remaining(self):
        for rec in self:
            rec.remaining = rec.no_of_employees - rec.assessed