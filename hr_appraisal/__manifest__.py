#-*- coding:utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Appraisal',
    'category': 'Human Resources',
    'author': 'OS34',
    'sequence': 0,
    'description': """
Custom Appraisal system.
=======================

    * Probation Appraisal
    * Annual Appraisal
    * Employee Appraisal Details
    * Manager Appraisal Details
    * Appraisal Template
    * Appraisal Notification
    """,
    'depends': [
        'web_readonly_bypass',
        'hr',
        'hr_contract',
        'project',
    ],
    'data': [
        'security/hr_appraisal_security.xml',
        'security/ir.model.access.csv',
        'data/hr_appraisal_cron.xml',
        'data/hr_appraisal_data.xml',
        'views/appraisal_template_view.xml',
        'views/appraisal_details_view.xml',
        'views/hr_appraisal_view.xml',
        'views/appraisal_menu.xml',
    ],
}
